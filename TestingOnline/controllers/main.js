import { questionArr } from "../data/data.js";
import { renderQuestion } from "./controller.js";

console.log(questionArr);

let currentIndex = 0;

// khi user load trang
renderQuestion(questionArr[currentIndex]);

// khi user nhấn câu tiếp theo
let nextQuestion = () => {
  currentIndex++;
  renderQuestion(questionArr[currentIndex]);
};
window.nextQuestion = nextQuestion;
