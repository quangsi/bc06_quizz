let renderSingleChoice = (question) => {
  console.log("loai 1");

  let contentHTML = "";
  question.answers.forEach((item) => {
    contentHTML += `<div class="form-check">
        <label class="form-check-label">
        <input type="radio" class="form-check-input" name="singleChoice" id="" value="checkedValue" >
       ${item.content}
      </label>
    </div>
    `;
  });
  return contentHTML;
};

let renderFillInput = (question) => {
  return ` <div class="form-group">
   <input type="text"
     class="form-control" id="" placeholder="">
 </div>`;
};

export let renderQuestion = (question) => {
  let { questionType } = question;
  let contentQuestion;
  if (questionType == 1) {
    contentQuestion = renderSingleChoice(question);
  } else {
    contentQuestion = renderFillInput(question);
  }
  document.getElementById("contentQuiz").innerHTML = `
  <h2>${question.content}</h2>
  ${contentQuestion}
  `;
};
